# mps_autofaces: Face detection for MyPhotoShare

`mps_autofaces` command enhances [MyPhotoShare](https://gitlab.com/paolobenve/myphotoshare) photo gallery by automatically
recognizing known people in pictures.

## Usage

`mps_autofaces` parses [MyPhotoShare](https://gitlab.com/paolobenve/myphotoshare) photos from root directory to learn
names of known people from sample pictures and then recognize these persons in the remaining pictures. It does so
using [face_recognition](https://github.com/ageitgey/face_recognition) Python module.

The known persons are defined in MyPhotoShare `album.ini` files using the `known_person` tag. There must be only one
known person per picture for `mps_autofaces` to learn the name of these persons, but there must be multiple sample pictures
identifying the same person for better recognition results.

When `mps_autofaces` has learned the know people from the samples, it then fetches the remaining pictures defined
in `album.ini` files and tries to recognize these persons into these pictures. When it recognizes people, it adds
`auto_faces` tags with the name of the persons present in the picture.

Existing `auto_faces` values are replaced by the new values.

### Command line

```sh
mps_autofaces <Samples root directory> [<Albums root directory>]
```

The `<Samples root directory>` is the path name of the root directory tree where `mps_autofaces` look for `album.ini`
files to learn known people.

The optional `<Albums root directory>` is the path name of MyPhotoShare root albums where `mps_autofaces` will look for
pictures with person to recognize and will update the album files.

If only one argument is given, `mps_autofaces` assumes that the Samples and Albums root directory are the same.

`mps_autofaces` is `album.ini`-guided! `mps_autofaces` scans the whole directories structure for `album.ini` files.
If a directory contains pictures but no `album.ini` file, it is not considered and the pictures won't be processed.
You can create `album.ini` files from content in advance using `myphotoshare_make_album_ini` under Debian/Ubunto
or `make_album_ini.sh` command.

### Options

`--album-ini`       : Name of the user-defined pictures metadata file replacing MyPhotoShare default 'album.ini'.
`--no-update`       : Don't write generated keywords into `<Albums root directory>` `album.ini` files.
`--stats`           : Print simple learning statistics.
`--tolerance`       : Tolerance of recognition accuracy (default=0.6).

### Example

Suppose that directory `/usr/share/local/media` contains the following `album.ini` file:

```ini
[Alice.jpg]
title = An ID snaphot of Alice
known_person = Alice Wonderland

[Gordon.jpg]
title = Gordon at work
known_person = Gordon Zola

[Garden party.jpg]
title = A lot of people was present at the garden party
```

After running `mps_autofaces /usr/share/local/media`, this `album.ini` file becomes:

```ini
[Alice.jpg]
title = An ID snaphot of Alice
known_person = Alice Wonderland

[Gordon.jpg]
title = Gordon at work
known_person = Gordon Zola

[Garden party.jpg]
title = A lot of people was present at the garden party
auto_faces = Alice Worderland, Gordon Zola
```

`mps_autofaces` has been able to recognize Alice and Gordon from the people present in the `Garden party.jpg` picture.

## Install

### Create virtual environment

```sh
$ sudo apt install python3-venv

# Create Python 3 virtual environment 'venv'
$ python3 -m venv venv

# Activate venv
$ source venv/bin/activate

# Upgrade pip
(venv) $ pip install --upgrade pip
```

### Install dlib

```sh
# Install pre-requisites:
$ sudo apt install build-essential \
    cmake \
    gfortran \
    git \
    wget \
    curl \
    graphicsmagick \
    libgraphicsmagick1-dev \
    libatlas-base-dev \
    libavcodec-dev \
    libavformat-dev \
    libgtk2.0-dev \
    libjpeg-dev \
    liblapack-dev \
    libswscale-dev \
    pkg-config \
    python3-dev \
    python3-numpy \
    software-properties-common \
    zip

$ sudo apt clean

# Get latest version of dlib
$ git clone https://github.com/davisking/dlib.git

# Build dlib
$ cd dlib
$ mkdir build
$ cd build

# With a recent computer, use AVX or SSE instructions
# cmake .. -DUSE_AVX_INSTRUCTIONS=ON
# cmake .. -DUSE_SSE4_INSTRUCTIONS=ON
# Or even better, if you have a GPU, search on the Internet how to compile it with CUDA support.
$ cmake ..
$ cmake --build . --config Release

# Python extension
$ cd ..
$ python3 setup.py install
```

From there, you can check you can import `dlib` in Python:

```sh
$ python3
Python 3.8.5 (default, Jul 28 2020, 12:59:40) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import dlib
>>> exit()
```

### Install face_recognition

```sh
# Install face recognition
$ cd ..
$ pip install face_recognition
```

### Install sklean

```sh
$ pip install sklearn
```

## Best practices

Because `mps_autofaces` is very good at recognizing people with very few examples, I've found that it is easier to
maintain a separate directory containing the pictures of the known persons. The pictures there can be tuned (i.e. cropped or
resized) to fit `mps_autofaces` learning process. Also the `album.ini` files there can be minimal as the only required tag
is `known_person`.

When you want to enhance the capacity of `mps_autofaces`, just drop a few new pictures into that directory and update the
corresponding `album.ini` file. You don't need to provide high resolution pictures, but portrait picture where the face of
the person is complete. She should not ware sunglasses or a hat.

When `mps_autofaces` prints an error message saying that it can't detect a person in a training example, replace the picture
by another one.

Of course, I maintain this examples directory outside of MyPhotoShare albums so that it does not show in the web pages.

## How to improve recognition?

1. First make sure that the pictures of the `known_person`s are correctly oriented. Mobile phones store the orientation of the
   picture in the EXIF data but `mps_autofaces` expects the picture to have the right orientation. In that case, open your favorite
   photo editor and rotate the picture.

   And more generally, all pictures where you want to recognize people must be correctly oriented else the recognition will fail.

1. You can provide multiple pictures of the same person. This will improve the accuracy of the recognition. Use pictures of that
   person at different ages or different attitudes.

1. Use color pictures instead of black and white.

## Known bugs

* `mps_autofaces` is able to find multiple occurrence of a person in a single picture. We filter the results
  to keep only one occurrence. I suppose it can be a bug in case of twins...
* `mps_autofaces` works reliably when the person faces the camera. It does not recognize people from their profile
  or their back.
* `mps_autofaces` can be too much precise and detects multiple faces in a picture where a human being focus only on
  one. Check that the person is not holding a book with pictures of people or similar small details.
* Old `album.ini` files generated with `make_album_ini.sh` command started with comments explaining the syntax of tags
  that can be used in the file. Because `mps_autofaces` updates `album.ini` files and tries to keep comments into them
  using a Python `ConfigParser` [work around](https://stackoverflow.com/questions/21476554/update-ini-file-without-removing-comments),
  these syntax comments are seen as incorrect syntax. A new section `[_DOCUMENTATION_]` has been created above these
  syntax comments in new `album.ini` files. But `mps_autofaces` fails when it encounters an old file. To correct them,
  either run `make_album_ini.sh` (*MyPhotoShare v5+*) in the directory or add a `[_DOCUMENTATION_]` first line in the file.
  
  Old `album.ini` files usually started with:

  ```ini
  # User defined metadata for MyPhotoShare
  ########################################
  # Possible metadata = 
  # - title = To give a title to the photo, video or album.
  # - description = A long description of the media.
  ...
  ```

  New `album.ini` files compatible with `mps_autofaces` starts with:

  ```ini
  [_DOCUMENTATION_]
  # User defined metadata for MyPhotoShare
  ########################################
  # Possible metadata = 
  # - title = To give a title to the photo, video or album.
  # - description = A long description of the media.
  ...
  ```

## Script `make_known_person.sh`

The bash script `make_known_person.sh` can be used to create the `album.ini` file required for learning faces.
Given a directory with training pictures named with the pattern `Person name-Suffix.jpg`, it creates a minimal
`album.ini` file with all the pictures and `knwon_person = Person name` values.

Use it like `$ make_known_person.sh <Samples root directory>`. The `-c` option can be used to specify an
alternate MyPhotoShare configuration file.
