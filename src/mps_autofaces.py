
import sys
import os
import argparse
import time
import string
import configparser
import math
import pickle
from collections import Counter
import imghdr

import face_recognition
import numpy as np

from sklearn import svm


# Tolerance in accuracy
TOLERANCE = 0.6

# Directory where to save learned data
DATADIR = os.path.normpath(os.path.join(os.path.realpath(__file__), "../../data"))

# Encodings pickle file
FILE_ENCODINGS = "face-encodings.pkl"


def main():
    """
    The main dispatcher.
    mps_autotags is run in 3 phases:
        1. Prepare the datasets, pictures and vocabulary (synsets).
        2. Train a neural network to recognize features in pictures.
        3. Let the neural network recognize features in a new set of pictures and generate the keywords.
    """
    parser = argparse.ArgumentParser(description="Automatic person recognition indexer for MyPhotoShare",
            usage="""mps_autofaces [--options] <Examples> [<Albums>]
Automatically recognize people in albums pictures after learning their name from
sample pictures, for use in MyPhotoShare gallery.

`<Examples>` is the root of a directory tree where `mps_autofaces` looks for `album.ini`
files where it can learn people names from sample pictures.

`<Albums>` is the optional root of a directory tree, typically MyPhotoShare root album, where
`mps_autofaces` updates the `album.ini` files adding `auto_faces` tags with the names
of people it has recognized in the pictures.

If `<Albums>` is not provided, it is assumed to be equal to `<Examples>`.

Type `mps_autofaces command --help` for command options.
""")
    parser.add_argument("examples", default='/usr/local/share/media', help="Directory tree containing album.ini files with identified known persons.")
    parser.add_argument("albums", nargs='?', help="Optional albums tree where mps_autofaces updates album.ini files with recognized persons.")
    parser.add_argument("--data-dir", default=DATADIR, help="Directory where mps_autofaces saves its data files (default={})".format(DATADIR))
    parser.add_argument("--album-ini", help="Name of the user-defined pictures metadata file replacing MyPhotoShare default 'album.ini'.")
    parser.add_argument("--no-update", action='store_true', help="Don't write generated keywords into <Albums> `album.ini` files.")
    parser.add_argument("--stats", action='store_true', help="Print simple learning statistics.")
    parser.add_argument("--tolerance", type=float, default=TOLERANCE, help="Tolerance of recognition accuracy (default={}).".format(TOLERANCE))
    parser.add_argument("--load-encodings", action='store_const', const=FILE_ENCODINGS, help="Read previously saved learned faces instead of reading <Examples> files.")

    args = parser.parse_args()

    examples = args.examples
    if not os.path.isdir(examples):
        logerr("{} is not a valid directory".format(examples))
        exit(1)

    if args.albums is None:
      albums = examples
    else:
      albums = args.albums
    if not os.path.isdir(albums):
        logerr("{} is not a valid directory".format(albums))
        exit(1)

    album_ini = 'album.ini'
    if args.album_ini is not None:
        album_ini = args.album_ini

    if args.tolerance < 0.0 or args.tolerance >= 1.0:
        logerr("Tolerance value must be in range ]0.0 .. 1.0[.")
        exit(1)

    file_encodings = os.path.join(args.data_dir, FILE_ENCODINGS)
    if args.load_encodings is not None:
        if os.path.isfile(file_encodings):
            with open(file_encodings, 'rb') as features_file:
                try:
                    d = pickle.load(features_file)
                    examples_album_ini = d['examples_album_ini']
                    encodings = d['encodings']
                    names = d['names']
                    classifier = d['classifier']
                except ValueError:
                    logerr("Face encodings file '{}' is corrupted. Run mps_autofaces without --load-encodings to recreate it.".format(file_encodings))
                    exit(1)
        else:
            logerr("Can't find encodings file '{}'. Run mps_autofaces without --load-encodings to recreate it.".format(file_encodings))
            exit(1)
    else:
        # Learn to recognize persons from example pictures.
        examples_album_ini, encodings, names = learn_examples(args, examples, album_ini)

        # Prepare classifier
        classifier = svm.SVC(gamma='scale', class_weight='balanced')
        classifier.fit(encodings, names)

        # Save encodings for future use
        log("Saving face encodings into {}".format(file_encodings))
        features = {'examples_album_ini': examples_album_ini, 'encodings': encodings, 'names': names, 'classifier': classifier}
        with open(file_encodings, 'wb') as features_file:
            pickle.dump(features, features_file)

    if len(names) == 0:
        logerr("There are no known-persons defined in {} that can be used for recognition. Check that the albums contain `album.ini` files with `known_person` definitions.".format(examples))
        exit(1)

    # Print statistics
    if args.stats:
        print_stats(examples_album_ini, album_ini, names)

    # Recognize people in picture albums from learned encodings.
    recognize(args, albums, album_ini, encodings, names, classifier)


def log(msg):
    """
    Print a message.
    """
    print("{} - {}".format(int(time.process_time()), msg))


def logerr(msg):
    """
    Print an error message.
    """
    print("{} - ERROR: {}".format(int(time.process_time()), msg), file=sys.stderr)


def find_albums_ini(rootdir, album_ini):
    """
    From the `rootdir` root directory, search for the `album_ini` files and
    return all the paths in a list.
    """
    albums_ini = []
    for root, _, filenames in os.walk(rootdir):
        for filename in filenames:
            if filename == album_ini:
                albums_ini.append(os.path.join(root, filename))

    return albums_ini


def print_stats(albums_ini, album_ini, names):
    """
    Print statistics on learned persons.
    """
    log("Found {} {} files".format(len(albums_ini), album_ini))
    counter = Counter(names)
    persons = counter.items()
    log("Learned to recognize {} known persons from {} pictures".format(len(persons), len(names)))
    for name, count in persons:
        log("  {} {} times".format(name, count))


def learn_examples(args, examples, album_ini):
    """
    Learn to recognize persons from example pictures.
    """
    log("Learning known persons from {} root album".format(examples))

    # Find all album.ini files to process
    albums_ini = find_albums_ini(examples, album_ini)

    # From these album.ini files, find all pictures with identified persons
    encodings, names = learn_persons(albums_ini)

    return albums_ini, encodings, names


def learn_persons(albums_ini):
    """
    Look for the know persons in the list of `album.ini` files and create
    their face encodings.
    """
    encodings = []
    names = []

    for album_ini in albums_ini:
        inifile = configparser.ConfigParser(allow_no_value=True)
        inifile.optionxform = str
        inifile.read(album_ini)
        path, _ = os.path.split(album_ini)

        for picture_name in inifile.sections():
            image_file = os.path.join(path, picture_name)

            if 'known_person' in inifile[picture_name] and os.path.isfile(image_file):
                person_name = inifile[picture_name]['known_person']
                log("Learning to recognize {}".format(person_name))

                # Now analyze the picture of that person to get its encodings.
                face = face_recognition.load_image_file(image_file)

                # With the examples, we use a CNN model to find the face in the picture
                #face_bounding_boxes = face_recognition.face_locations(face, model='cnn')
                face_bounding_boxes = face_recognition.face_locations(face)

                #If training image contains exactly one face
                if len(face_bounding_boxes) == 1:
                    face_encodings = face_recognition.face_encodings(face, num_jitters=100)[0]
                    # Add face encoding for current image with corresponding label (name) to the training data
                    encodings.append(face_encodings)
                    names.append(person_name)
                else:
                    logerr("{} (known_person = {}) contains more than one person and can't be used for training".format(image_file, person_name))

    return encodings, names


def recognize(args, albums, album_ini, encodings, names, classifier):
    """
    Recognize people in picture albums from learned encodings.
    """
    log("Start persons identification from {} root album".format(albums))

    # Search the album.ini files to analyze.
    albums_ini = find_albums_ini(albums, album_ini)

    for ini in albums_ini:
        inifile = configparser.ConfigParser(allow_no_value=True, comment_prefixes='|')
        inifile.optionxform = str
        try:
            inifile.read(ini)
        except configparser.MissingSectionHeaderError:
            logerr("Found {} with old `album.ini` syntax. Not considering for people recognition. Update the file with `make_album_ini.sh`".format(ini))
            continue
        path, _ = os.path.split(ini)

        # For each picture in the album.ini that is not a known_person, try to detected people.
        for picture_name in inifile.sections():
            if picture_name != 'album' and picture_name != 'DEFAULT':
                auto_faces = []

                image_file = os.path.join(path, picture_name)
                # If we have an image file
                if os.path.isfile(image_file) and imghdr.what(image_file) is not None:
                    # That's not a reference person: try to find who he is.
                    if 'known_person' not in inifile[picture_name]:
                        image = face_recognition.load_image_file(image_file)

                        # Find all the faces in the picture using the HOG-based model (HOG = Histogram of Oriented Gradients)
                        faces_bounding_boxes = face_recognition.face_locations(image)
                        nb = len(faces_bounding_boxes)
                        log("Detected {} faces in picture {}".format(nb, image_file))

                        # Predict all the faces in the image
                        if nb > 0:
                            first = True
                            for i in range(nb):
                                face_encodings = face_recognition.face_encodings(image, known_face_locations=faces_bounding_boxes)[i]
                                name = classifier.predict([face_encodings])[0]
                                distances = face_recognition.face_distance(encodings, face_encodings)
                                # i_person is the index of the person with the shortest distance to the picture.
                                i_person = np.argmin(distances)
                                # The person found by classifier agrees with euclidiean distance, we have a candidate
                                if names[i_person] == name and distances[i_person] <= args.tolerance:
                                    auto_faces.append(names[i_person])
                                    if first:
                                        first = False
                                        #log("  Found with distance <= {:.4f} from known persons:".format(args.tolerance))
                                    log("  FOUND #{} {} (dist={:.4f})".format(i, names[i_person], distances[i_person]))
                                elif names[i_person] != name:
                                    log("  DISAG #{} Classifier found {} while distance found {} (dist={:.4f})".format(i, name, names[i_person], distances[i_person]))
                                else:
                                    log("  TOLER #{} {} (dist={:.4f}) is not within tolerance".format(i, name, distances[i_person]))

                            if first:
                                log("  NOPER I was not able to recognize a known person in that picture...")

                    else:
                        # That's a reference person: we know who he is.
                        log("Known person in {}".format(image_file))
                        auto_faces = [inifile[picture_name]['known_person']]

                    # Keep the recognized persons
                    inifile[picture_name]['auto_faces'] = ', '.join(set(auto_faces))

        # If the user wants to keep the updated `album.ini` file, write it to disk.
        if not args.no_update:
            log("Saving recognized persons into {}".format(ini))
            with open(ini, 'w') as album_file:
                inifile.write(album_file)


# Let's go!
main()