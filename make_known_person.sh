#!/bin/bash
###########
# Create a MyPhotoShare 'album.ini' file based on media content files in a directory for mps_autofaces
# but to be used *only* as examples 'album.ini'.
# If an 'album.ini' already exists, it is replaced. Pictures are alphabetically sorted in that file.
# Name of metadata file (default 'album.ini') is read from config file (default /etc/myphotoshare/myphotoshare.conf)

# All media file extensions that will be considered:
# jpg,jpeg,JPG,JPEG,png,PNG

if [ -z "$BASH_VERSION" ]; then
	( >&2 echo "This script must run with bash shell. It does not work with dash!" )
	( >&2 echo "Under Ubuntu, don't use '$ sh $0 $*' to run it.")
	( >&2 echo "Simply do '$ $0 $*'.")
	exit 1
fi

print_usage()
{
	( >&2 echo "Usage: $0 [ -c CONFIG_FILE ] FOLDER" )
	( >&2 echo "Create '$ALBUM_INI' file in 'FOLDER' based on its media content for 'mps_autofaces' training use." )
	( >&2 echo "See mps_autofaces (https://gitlab.com/pmetras/mps_autofaces) for more information.">> "$DIR/$ALBUM_INI" )
	( >&2 echo )
	( >&2 echo "The 'FOLDER' directory must contains pictures of people, one person by picture file, that are" )
	( >&2 echo "used as training faces for 'mps_autofaces' learning phase. '$0' will use the picture name" )
	( >&2 echo "as the 'known_person' tag value, eventually removing a dash-suffix." )
	( >&2 echo )
	( >&2 echo "This '$ALBUM_INI' contains *only* 'known_person' tags; use 'make_album_ini.sh' command to create" )
	( >&2 echo "a complete '$ALBUM_INI' template file for use with MyPhotoShare." )
	( >&2 echo "If the file '$ALBUM_INI' already exists in 'FOLDER', it is replaced by a new one." )
	( >&2 echo "The name of the metadata file '$ALBUM_INI' is taken from configuration file CONFIG_FILE." )
	( >&2 echo )
	( >&2 echo "Options:" )
	( >&2 echo "   -c CONFIG_FILE: Define path and name of the configuration file else" )
	( >&2 echo "   will use '/etc/myphotoshare/myphotoshare.conf'." )
	( >&2 echo )
	( >&2 echo "Example:" )
	( >&2 echo "   $0 ~/Training_faces" )
	( >&2 echo "   Create file '$ALBUM_INI' in '~/Training_faces' with 'known_person' tags for all" )
	( >&2 echo "   pictures contained in that folder. '$ALBUM_INI' name is read from" )
	( >&2 echo "   '/etc/myphotoshare/myphotoshare.conf'." )
	( >&2 echo )
	( >&2 echo "   If the picture 'Jonh Doe-12.jpeg' is present in that directory, the following entry is created" )
	( >&2 echo "   into the '$ALBUM_INI' file:" )
	( >&2 echo "   [Jonh Doe-12.jpeg]" )
	( >&2 echo "   known_person = Jonh Doe" )
}


# Set $CONF with configuration filename
set_config()
{
	# Already defined and valid: we stop there
	if [ -f "$CONF" ]; then
		return
	fi
	# Try to use parameter
	if [ -f "$1" ]; then
		CONF="$1"
	fi
	# Else use default
	if [ -z "$CONF" ]; then
		CONF="/etc/myphotoshare/myphotoshare.conf"
	fi
	# Stop if nothing worked
	if [ ! -f "$CONF" ]; then
		( >&2 echo "Error: Configuration file '$CONF' does not exist." )
		exit 1
	fi
}


# Get metadata filename from configuration and set $ALBUM_INI
set_album_ini()
{
	# Get metadata filename from configuration
	if [ -f "$CONF" ]; then
		ALBUM_INI="$(sed -nr 's/^\s*metadata_filename\s*=\s*((\w|\.)+)\s*.*$/\1/p' "$CONF")"
	fi
	# If not set, use defaults
	if [ -z "$ALBUM_INI" ]; then
		ALBUM_INI="album.ini"
	fi
}


# Process options
while getopts "c:" option; do
	case $option in
		c)
			set_config "$OPTARG"
		;;
		\?)
			set_config
			set_album_ini
			print_usage
			exit 1
		;;
	esac
done
shift $((OPTIND-1))

# Process parameters
DIR=${1%/}

set_config
set_album_ini

if [ -z "$DIR" ]; then
	print_usage
	exit 1
elif [ ! -d "$DIR" ]; then
	( >&2 echo "Argument must be a directory containing example pictures for faces learning." )
	exit 1
fi

echo "Create file '$DIR/$ALBUM_INI'."
echo "[_DOCUMENTATION_]" > "$DIR/$ALBUM_INI"
echo "# MyPhotoShare training album.ini for mps_autofaces " >> "$DIR/$ALBUM_INI"
echo "###################################################" >> "$DIR/$ALBUM_INI"
echo "# mps_autofaces (https://gitlab.com/pmetras/mps_autofaces).">> "$DIR/$ALBUM_INI"
echo "# Contains *only* 'known_person' tags!" >> "$DIR/$ALBUM_INI"
echo "# Don't use as general album.ini file." >> "$DIR/$ALBUM_INI"
echo "# Use 'make_album_ini.sh' instead." >> "$DIR/$ALBUM_INI"
echo >> "$DIR/$ALBUM_INI"

# Count the number of media added
SECTION_COUNT=0

# Loop on examples album content
SAVEIFS="$IFS"
IFS=$(echo -en "\n\b")
for media in $(ls "$DIR"/*.{jpg,jpeg,JPG,JPEG,png,PNG} 2> /dev/null); do
	SECTION=${media##*/}
	NAME=${SECTION%-*}
	echo "[$SECTION]" >> "$DIR/$ALBUM_INI"
	echo "known_person = $NAME" >> "$DIR/$ALBUM_INI"
	echo >> "$DIR/$ALBUM_INI"
	((SECTION_COUNT+=1))
done
IFS=$SAVEIFS

# Print number of media added in 'album.ini'
echo "$SECTION_COUNT face pictures added to '$DIR/$ALBUM_INI'."

